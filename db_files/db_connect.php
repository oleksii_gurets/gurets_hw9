<?php
try {
    $dbcon = new PDO('mysql:host=localhost;dbname=users', 'root', 'root');
    $dbcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbcon->exec('SET NAMES "utf8"');
} catch (PDOException $error) {
    die('No database connection! ' . $error->getMessage());
}
?>