<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/db_files/db_connect.php';
try {
    $dbcon->exec("CREATE TABLE users (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255),
        surname VARCHAR(255),
        age INT,
        email VARCHAR(255),
        phone VARCHAR(255)
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;");
} catch (PDOException $e) {
    echo 'Error creating TABLE: users<br>';
	echo $e->getMessage();
	echo '<br><a href="/">На главную</a>';
	die();
}
echo 'TABLE users created succesfully!<br>';
echo '<a href="/">Back to main page</a>';
die();
?>