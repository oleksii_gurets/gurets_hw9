<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/db_files/db_connect.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/classes/user.php';

try{
    if(empty($_GET['id'])){
        header('Location:/');
    }
    $id = (int)$_GET['id'];
    $pickedUser = User::pickUser($id, $dbcon);
    
}catch(PDOException $errorPicked){
    die('Error getting chosen user info!<br>'.$errorPicked->getMessage());
}
?>

<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php';?>
<div class="container">
    <div class="row">
        <h2>User name: <?= $pickedUser->name?></h2>
        <h2>User surname: <?= $pickedUser->surname?></h2>
        <p class="fs-3 fst-italic">User age: <?= $pickedUser->age?></p>
        <p class="fs-3 text-uppercase">User email: <?= $pickedUser->email?></p>
        <p class="fs-3 text-uppercase">User phone: <?= $pickedUser->phone?></p>
        <a href="/" class="pagination" class="page-link"><<<</a>
    </div>  
</div>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';?>