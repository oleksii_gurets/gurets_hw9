<?php
include_once(dirname(__FILE__) . '/db_files/db_connect.php');
include_once(dirname(__FILE__) . '/classes/user.php');

if(empty($_POST['name']) || empty($_POST['surname']) || empty($_POST['age']) || empty($_POST['email'])) {
    header('Location:/templates/create_form.php');
}

$name = htmlspecialchars($_POST['name'], ENT_QUOTES, 'UTF-8');
$surname = htmlspecialchars($_POST['surname'], ENT_QUOTES, 'UTF-8');
$age = htmlspecialchars($_POST['age'], ENT_QUOTES, 'UTF-8');
$email = htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8');
$phone = htmlspecialchars($_POST['phone'], ENT_QUOTES, 'UTF-8');

$newUser = new User($name, $surname, $age, $email, $phone);
$newUser->saveUser($dbcon);
header('Location:/?notification=entry_saved');   //Посылаем GET параметр для уведомления об успешном создании пользователя
?>