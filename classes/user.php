<?php
class User 
{
    public $id = 0;
    public $name = "";
    public $surname = "";
    public $age = 0;
    public $email = "";
    public $phone = null;

    public function __construct($name, $surname, $age, $email, $phone = null, $id = null) {   //phone можем добавлять опционально
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->age = $age;
        $this->email = $email;
        $this->phone = $phone;
    } 

    public static function isDBtableExists(PDO $pdoConnect, $tableName) {      //Функция проверки существования таблицы users в БД
        try {
            $testVar = $pdoConnect->query("SELECT 1 FROM " . $tableName . " LIMIT 1");
        } catch (PDOException $e) {
            return false;
        }
        return true;
    }

    public function saveUser(PDO $dbcon) {                         //Функция создания записи нового пользователя в таблице
		try {
			$sql ="INSERT INTO users SET
			name=:name,
			surname=:surname,
			age=:age,
			email=:email,
            phone=:phone
			";
			$state = $dbcon->prepare($sql);
			$state->bindValue(':name', $this->name);
			$state->bindValue(':surname', $this->surname);
			$state->bindValue(':age', $this->age);
			$state->bindValue(':email', $this->email);
            $state->bindValue(':phone', $this->phone);
			$state->execute();
		} catch (PDOException $errorSave) {
			die('Error creating a new user!<br>' . $errorSave->getMessage());
		}
	}

    public static function allUsersInfo(PDO $dbcon) {           //Функция получения всех пользователей из БД
        try {
            $sql = "SELECT * FROM users";
            $gottenObject = $dbcon->query($sql);
            $usersArray = $gottenObject->fetchAll();
            $usersList = [];
            foreach($usersArray as $user) {
                $usersList[] = new User($user['name'], $user['surname'], $user['age'], $user['email'], $user['phone'], $user['id']);
            }
            return $usersList;
        } catch (PDOException $errorUsers){
            die('Error getting users!<br>' . $errorUsers->getMessage());
        }
    }

    public static function pickUser($id, PDO $dbcon) {           //Функция для получения информации по конкретному пользователю
        try {
			$sql = "SELECT * FROM users WHERE id = :id;";
			$state = $dbcon->prepare($sql);
			$state->bindValue(':id', $id);
			$state->execute();
			$userArray = $state->fetchAll();
			return new self($userArray[0]['name'], $userArray[0]['surname'], $userArray[0]['age'], $userArray[0]['email'], $userArray[0]['phone'], $userArray[0]['id']);
		} catch (PDOException $errorPick){
			die('Error picking current user!<br>' . $errorPick->getMessage());
		}
    }

    static public function deleteUser($id, PDO $dbcon) {          //Функция удаления пользователей из БД
        try {
            $sql = "DELETE FROM users WHERE id=:id";
            $state = $dbcon->prepare($sql);
            $state->bindValue(':id', $id);
            $state->execute();
        } catch (Exception $errorDelete) {
            die('Error deleting users!<br>'. $errorDelete->getMessage());
        }
    }
}
?>