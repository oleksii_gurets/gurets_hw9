<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/header.php';?>
    <div class="container">
        <div class="row">
            <h1>Create a new user, enter necessary user information</h1>
            <form action="/create_user.php" method="POST" class="w-50 p-3" style="background-color: tan;">
                <div class="mb-3">
                    <label for="name" class="form-label">User name:</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>
                <div class="mb-3">
                    <label for="surname" class="form-label">User surname:</label>
                    <input type="text" class="form-control" name="surname" id="surname">
                </div>
                <div class="mb-3">
                    <label for="age" class="form-label">User age:</label>
                    <input type="text" class="form-control" name="age" id="age">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">User email:</label>
                    <input type="text" class="form-control" name="email" id="email">
                </div>
                <div class="mb-3">
                    <label for="phone" class="form-label">User phone:</label>
                    <input type="text" class="form-control" name="phone" id="phone">
                </div>
                <button type="submit" class="btn btn-secondary">Add user</button>
            </form>
        </div>
    </div>    
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';?>
