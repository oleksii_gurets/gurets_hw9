<div class="container">
    <div class="row">
    <form style="display:inline-block" method="POST" action="/delete_user.php">                
            <table class="table table-warning table-hover table-bordered border-primary">
                <thead>
                    <tr style="text-align: center;">
                        <th scope="col">Check</th>
                        <th scope="col">User first name</th>
                        <th scope="col">User second name</th>
                        <th scope="col">Detailed information</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($allUsers as $currentUser):?>
                        <tr style="text-align: center;">
                            <td>
                                <input class="form-check-input" type="checkbox" name="checkbox[]" value="<?=$currentUser->id?>" id="user">
                                <label class="form-check-label" for="user">
                            </td>
                            <td><?=$currentUser->name; ?></td>
                            <td><?=$currentUser->surname; ?></td>
                            <td><a href="/user_info.php?id=<?=$currentUser->id?>" class="btn btn-primary">Get more user info</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <button class="btn btn-danger">Delete picked user</button>
        </form>   
    </div>
</div>
