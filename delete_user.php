<?php
include_once(dirname(__FILE__) . '/db_files/db_connect.php');
include_once(dirname(__FILE__) . '/classes/user.php');

if(empty($_POST['checkbox'])) {  //Проверяем получение массива всех выбранных id пользователей
    header('Location:/');
}

foreach($_POST['checkbox'] as $id) {   //Перебираем полученный массив, вызывая функцию удаления пользователей
    User::deleteUser($id, $dbcon);
    header("Location:/");
}
?>