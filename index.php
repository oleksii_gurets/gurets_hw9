<?php
include_once(dirname(__FILE__) . '/db_files/db_connect.php');
include_once(dirname(__FILE__) . '/classes/user.php');
$tableName = 'users'; 
?>
<?php include_once(dirname(__FILE__) . '/templates/header.php');?>
    <div class="container">
        <div class="row">
            <?php if(User::isDBtableExists($dbcon, $tableName)):?>
                    <div class="alert alert-primary" role="alert">
                        Table 'users' exists!
                    </div>
                    <div class="mb-3">
                        <a href="/templates/create_form.php" class="btn btn-warning">Create new user</a> 
                    </div>
            <?php else:?>
                    <div class="mb-3">
                        <a href="/db_files/db_createtable.php" class="btn btn-warning">Create DB table</a> 
                    </div>
            <?php endif;?>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php if(!empty($_GET['notification'])):?>       
                <?php if($_GET['notification'] == 'entry_saved'):?>
                    <div class="alert alert-primary" role="alert">
                        A new user successfully added!
                    </div>
                <?php endif;?>
            <?php endif;?>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php if(!empty($allUsers = User::allUsersInfo($dbcon))):?>
                <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/display_delete_form.php';?>
            <?php endif;?>
        </div>
    </div>
<?php include_once(dirname(__FILE__) . '/templates/footer.php');?>
    